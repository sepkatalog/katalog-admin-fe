import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ItemMainComponent } from './item-main/item-main.component';


const routes: Routes = [
  {path :"",component:ItemMainComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ItemRoutingModule { }
