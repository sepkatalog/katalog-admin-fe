import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FormService {

  public formContainerSource = new BehaviorSubject(null);
  initialFormContainer = this.formContainerSource.asObservable();

  public afterFormSource = new BehaviorSubject(null);
  initialAfterFormSource = this.afterFormSource.asObservable();

  constructor() { }

  setFormComponent(data: any) {
    this.formContainerSource.observers = this.formContainerSource.observers.slice(this.formContainerSource.observers.length - 2);
    this.formContainerSource.next(data);
  }

  afterFormSubmit(data: any) {
    this.afterFormSource.observers = this.afterFormSource.observers.slice(this.afterFormSource.observers.length - 1);
    this.afterFormSource.next(data);
  }

}
