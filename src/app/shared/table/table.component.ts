import { Component, OnInit, ViewChildren, QueryList, Input, Output, EventEmitter } from '@angular/core';
import { DecimalPipe } from '@angular/common';

// import { tableData } from './data';

import { TableService } from './table.service';
import { TableSortableDirective, SortEvent } from './table-sortable.directive';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
  providers: [TableService, DecimalPipe]
})

/**
 * Advanced table component - handling the advanced table with sidebar and content
 */
export class TableComponent implements OnInit {

  @Input() tableData: any[] = [];
  @Input() pageSize = 10;
  @Input() totalRecords: number = 0;
  @Output() pageChange = new EventEmitter();
  @Output() buttonClick = new EventEmitter();
  @Output() editEvent = new EventEmitter();
  @Output() viewEvent = new EventEmitter();
  @Output() deleteEvent = new EventEmitter();
  @Output() playEvent = new EventEmitter();
  @Output() searchTerm = new EventEmitter();

  @Input() isButton = false;
  @Input() isSearch = false;
  @Input() isPlay = false;
  @Input() buttonTitle: string;
  @Input() columns: any[];
  @Input() title: string;
  @Input() isActions = true;
  @Input() isImage = false;
  @Input() pageNumber = 0;
  @Input() isDelete = true;
  @Input() isView = true;
  isShowLoading = false;

  @ViewChildren(TableSortableDirective) headers: QueryList<TableSortableDirective>;

  constructor(
  ) { }

  ngOnInit() {
  }
  /**
   * Sort table data
   * @param param0 sort the column
   *
   */
  onSort({ column, direction }: SortEvent) {
    // resetting other headers
    this.headers.forEach(header => {
      if (header.sortable !== column) {
        header.direction = '';
      }
    });
  }
  sendClickAlert() {
    this.buttonClick.emit();
  }
  emitEditEvent(event) {
    this.editEvent.emit({ data: event });
  }
  emitViewEvent(event) {
    this.viewEvent.emit({ data: event });
  }
  emitDeleteEvent(event) {
    this.deleteEvent.emit({ data: event });
  }
  emitPlayEvent(event) {
    this.playEvent.emit({ data: event });
  }
  pageChangeEvent(event) {
    this.pageNumber = event;
    // this.pageChange.emit({page:this.pageNumber,size:this.pageSize,isPrv:false});
    // this.loadData();
  }
  nextPage() {
    this.pageChange.emit({ page: this.pageNumber + 1, size: this.pageSize, isPrev: false });
  }
  prevPage() {
    if (this.pageNumber > 0) {
      this.pageChange.emit({ page: this.pageNumber - 1, size: this.pageSize, isPrev: true });
    }
  }
  search(event) {
    this.searchTerm.emit(event.target.value);
  }
}
