import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TableComponent } from './table/table.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {NgbPaginationModule, NgbProgressbarModule, NgbTypeaheadModule} from '@ng-bootstrap/ng-bootstrap';
import { PagetitleComponent } from './pagetitle/pagetitle.component';
import {TableSortableDirective} from './table/table-sortable.directive';



@NgModule({
  declarations: [
    TableComponent,
    PagetitleComponent,
    TableSortableDirective
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgbPaginationModule,
    NgbTypeaheadModule,
    NgbProgressbarModule
  ],
  exports: [TableComponent, TableSortableDirective]
})
export class SharedModule { }
