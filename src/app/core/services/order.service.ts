import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  api = `/api/v1`

  constructor(
    private httpClient: HttpClient
  ) { }

  getOrders(queryparams: string): Observable<any> {
    let url: string = `${this.api}/order${queryparams}`;
    return this.httpClient.get(url);
  }
}
