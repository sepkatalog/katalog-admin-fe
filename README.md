# README #

Katalog is a simple shopping cart developed especially for PlantEra requirement.

This application consists of 3 main repositories.

katalog-admin-be (All backend services are developed in this repository)

katalog-admin-fe (All frontend UI features related to administrator's web portal are developed in this repository)

katalog-customer-fe (All frontend UI features related to customer's web portal are developed in this repository)


### What is this repository for? ###

* All UI/UX features related to administrator portal are developed in this repository 
* Version 1.0

### How do I get set up? ###

* Clone the repository
* Checkout to the relavant branch based on the environment (DEV, QA, PROD). (for production, use the master branch)
* Change the directory to katalog-admin-fe
* Execute below commands: 
* npm install
* ng run start