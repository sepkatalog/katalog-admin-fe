import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormItemComponent } from './form-item/form-item.component';
import { ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [FormItemComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
  ],
  exports:[FormItemComponent],
  entryComponents :[FormItemComponent]
})
export class CustomFormModule { }
