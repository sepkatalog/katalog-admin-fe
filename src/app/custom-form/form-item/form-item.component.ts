import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { KeycloakService } from 'keycloak-angular';
import { Action } from 'src/app/core/models/action';
import { FormService } from 'src/app/core/services/form.service';
import { ItemService } from 'src/app/core/services/item.service';
import { ConfirmationDialogService } from 'src/app/views/dialog/confirmation-dialog.service';

@Component({
  selector: 'app-form-item',
  templateUrl: './form-item.component.html',
  styleUrls: ['./form-item.component.scss']
})
export class FormItemComponent implements OnInit {

  form: FormGroup = this.fb.group({
    id: [],
    title: [],
    category: [],
    description: [],
    code: [""],
    specification: [],
    thumbnail: [],
    price: [0.00],
    status: [""],
    createdBy: [""],
    image: [""],
    categoryId: []
  });
  categories:any[]=[];

  @Input() data: any;
  @Input() type: Action;

  constructor(
    private fb: FormBuilder,
    private itemService: ItemService,
    private formService: FormService,
    private keyCloakService: KeycloakService,
    private confirmationDialogService: ConfirmationDialogService
  ) { }

  ngOnInit() {
    // this.setUser();
    if (!!this.data) {
      let data = {
        id: this.data.id,
        title: this.data.title,
        category: this.data.category,
        description: this.data.description,
        specification: this.data.specification,
        thumbnail: this.data.thumbnail,
        price: this.data.price,
        status: this.data.status,
        createdBy: this.data.createdBy,
        code: this.data.code,
        image: this.data.image,
        categoryId: this.data.categoryId
      }
      this.form.setValue(data);
    }
    this.getCategory();
  }

  getCategory(){
    this.itemService.getCategory()
    .subscribe(
      (res)=>{
        this.categories = res;
      }
    );
  }

  get thumbnail() {
    return this.form.controls["thumbnail"].value
  }
  get image() {
    return this.form.controls["image"].value
  }

  setUser() {
    this.keyCloakService.loadUserProfile().then(
      (res) => {
        this.form.controls["createdBy"].setValue(res.username);
      }
    )
  }

  saveItem() {
    this.itemService.addItem(this.form.value)
      .subscribe(
        (res) => {
          document.body.classList.remove('right-bar-enabled');
          this.formService.afterFormSubmit(res);
        }
      );
  }

  updateItem() {
    this.confirmationDialogService.confirm('Update Item', 'Are you sure you want to update ?')
      .then((confirmed) => {
        if (confirmed) {
          this.itemService.updateItem(this.form.value)
            .subscribe(
              (res) => {
                // document.body.classList.remove('right-bar-enabled');
                this.formService.afterFormSubmit(res);
              }
            );
        }
      })
      .catch(() => console.log('User dismissed the dialog'));

  }

  setBase64Image(event, isThumbnail) {
    var file = event.target.files[0];
    var reader = new FileReader();
    let m = this;
    reader.onloadend = function () {
      if (isThumbnail) {
        m.form.controls["thumbnail"].setValue(reader.result);
      } else {
        m.form.controls["image"].setValue(reader.result);
      }
    }
    reader.readAsDataURL(file);
  }

  clearForm() {
    this.form.reset();
  }
  deleteItem() {
    this.confirmationDialogService.confirm('Delete Item', 'Are you sure you want to delete ?')
      .then((confirmed) => {
        if (confirmed) {
          this.itemService.deleteItem(this.form.value) //to change
            .subscribe(
              (res) => {
                document.body.classList.remove('right-bar-enabled');
                this.formService.afterFormSubmit(res);
              }
            );
        }
      })
      .catch(() => console.log('User dismissed the dialog'));
  }

  cancelForm() {
    document.body.classList.remove('right-bar-enabled');
  }
}

