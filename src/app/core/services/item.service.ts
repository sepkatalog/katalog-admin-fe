import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Item } from '../models/item.models';

@Injectable({
  providedIn: 'root'
})
export class ItemService {

  api = `/api/v1`

  constructor(
    private httpClient: HttpClient
  ) { }

  getItems(queryparams: string): Observable<any> {
    let url: string = `${this.api}/item${queryparams}`
    return this.httpClient.get(url);
  }
  addItem(payload: Item): Observable<Item> {
    return this.httpClient.post<Item>(`${this.api}/item`, payload);
  }
  deleteItem(payload: Item): Observable<Item> { 
    return this.httpClient.delete<Item>(`${this.api}/item/${payload.id}`);
  }
  updateItem(payload: Item): Observable<Item> {
    return this.httpClient.put<Item>(`${this.api}/item`, payload);
  }
  getCategory(): Observable<any> {
    return this.httpClient.get(`${this.api}/category`);
  }
}
