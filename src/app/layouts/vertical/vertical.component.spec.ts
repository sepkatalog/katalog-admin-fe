import { HttpClientModule } from '@angular/common/http';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { KeycloakService } from 'keycloak-angular';
import { FooterComponent } from '../shared/footer/footer.component';
import { LeftsidebarComponent } from '../shared/leftsidebar/leftsidebar.component';
import { MenuComponent } from '../shared/menu/menu.component';
import { TopbarComponent } from '../shared/topbar/topbar.component';

import { VerticalComponent } from './vertical.component';

describe('VerticalComponent', () => {
  let component: VerticalComponent;
  let fixture: ComponentFixture<VerticalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerticalComponent , TopbarComponent, LeftsidebarComponent, VerticalComponent, FooterComponent, MenuComponent],
      imports : [
        RouterTestingModule,
        HttpClientModule
      ],
      providers:[
        KeycloakService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerticalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
