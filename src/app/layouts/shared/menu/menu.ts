import { MenuItem } from './menu.model';

export const MENU: MenuItem[] = [
    {
        label: 'Navigation',
        isTitle: true
    },
    {
        label: 'Items',
        icon: 'home',
        link: '/item'
    },
    {
        label: 'Orders',
        icon: 'home',
        link: '/order'
    }
];
