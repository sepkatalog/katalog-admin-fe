import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[appAnchorForm]'
})
export class AnchorFormDirective {

  constructor(public viewContainerRef : ViewContainerRef) { }

}
