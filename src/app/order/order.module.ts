import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OrderRoutingModule } from './order-routing.module';
import { OrderHomeComponent } from './order-home/order-home.component';
import { SharedModule } from '../shared/shared.module';
import { UIModule } from '../shared/ui/ui.module';


@NgModule({
  declarations: [OrderHomeComponent],
  imports: [
    CommonModule,
    OrderRoutingModule,
    SharedModule,
    UIModule,
  ]
})
export class OrderModule { }
