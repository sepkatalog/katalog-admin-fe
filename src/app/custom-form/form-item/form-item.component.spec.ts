import { HttpClientModule } from '@angular/common/http';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { KeycloakService } from 'keycloak-angular';
import { of } from 'rxjs';
import { Item } from 'src/app/core/models/item.models';
import { ItemService } from 'src/app/core/services/item.service';
import { ConfirmationDialogService } from 'src/app/views/dialog/confirmation-dialog.service';

import { FormItemComponent } from './form-item.component';

describe('FormItemComponent', () => {
  let component: FormItemComponent;
  let fixture: ComponentFixture<FormItemComponent>;
  const keyCloakServiceMock: jasmine.SpyObj<KeycloakService> = jasmine.createSpyObj("KeyCloakService", ["loadUserProfile"]);
  const itemServiceMock: jasmine.SpyObj<ItemService> = jasmine.createSpyObj("ItemService", ["addItem", "getCategory","updateItem","deleteItem"]);
  const confirmationDialogService: jasmine.SpyObj<ConfirmationDialogService> = jasmine.createSpyObj("ConfirmationDialogService", ["confirm"]);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FormItemComponent],
      imports: [
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule
      ],
      providers: [
        { provide: KeycloakService, useValue: keyCloakServiceMock },
        { provide: ItemService, useValue: itemServiceMock },
        { provide: ConfirmationDialogService, useValue: confirmationDialogService }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormItemComponent);
    component = fixture.componentInstance;
    itemServiceMock.getCategory.and.returnValue(of([]));
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it("Should call saveItem", () => {
    itemServiceMock.addItem.and.returnValue(of({} as Item));
    component.saveItem();
    expect(itemServiceMock.addItem).toHaveBeenCalled();
  });
  it("Should call setUser", () => {
    keyCloakServiceMock.loadUserProfile.and.returnValue(of({ username: "test" }).toPromise());
    component.setUser();
    expect(keyCloakServiceMock.loadUserProfile).toHaveBeenCalled();
  });
  it("Should call thumbnail", () => {
    component.thumbnail;
    expect(component.thumbnail).toBe(null);
  });
  it("Should call thumbnail", () => {
    component.image;
    expect(component.image).toBe("")
  });
  it("Should call clearForm", () => {
    spyOn(component.form, "reset");
    component.clearForm();
    expect(component.form.reset).toHaveBeenCalled();
  });
  it("Should call getCategory", () => {
    component.getCategory();
    expect(itemServiceMock.getCategory).toHaveBeenCalled();
  });
  it("Should call updateItem", () => {
    itemServiceMock.updateItem.and.returnValue(of({} as Item));
    confirmationDialogService.confirm.and.returnValue(Promise.resolve(true))
    component.updateItem();
  })
  it("Should call deleteItem", () => {
    itemServiceMock.deleteItem.and.returnValue(of({} as Item));
    confirmationDialogService.confirm.and.returnValue(Promise.resolve(true))
    component.deleteItem();
  })
  it("Should call cancelForm",()=>{
    component.cancelForm();
  });
});
