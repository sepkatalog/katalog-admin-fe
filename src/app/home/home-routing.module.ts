import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../core/guards/auth.guard';
import { HomeComponent } from './home/home.component';


const routes: Routes = [
  { path: "home", component: HomeComponent },
  { path: "item", loadChildren: () => import('../item/item.module').then(m => m.ItemModule),canActivate: [AuthGuard]},
  { path: "order", loadChildren: () => import('../order/order.module').then(m => m.OrderModule),canActivate: [AuthGuard]},
  { path: "", redirectTo: "item" }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
