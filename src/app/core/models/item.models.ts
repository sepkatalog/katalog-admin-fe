export class Item{
    code: string;
	title: string;
	description: string;
	specification: string;
	thumbnail: string;
	image: string;
	price: number;
	category: number;
	status: string;
	createdBy: string;
	id!:number;
}