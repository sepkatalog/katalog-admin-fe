import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';

import { ConfirmationDialogComponent } from './confirmation-dialog.component';
import { ConfirmationDialogService } from './confirmation-dialog.service';

@NgModule({
    declarations: [
    ],
    imports: [
        BrowserModule,
        CommonModule
    ],
    exports: [
    ],
    providers: [
        ConfirmationDialogService
    ]
})
export class ConfirmDialogModule {
}  