import { APP_INITIALIZER, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ItemRoutingModule } from './item-routing.module';
import { ItemMainComponent } from './item-main/item-main.component';
import { SharedModule } from '../shared/shared.module';
import { UIModule } from '../shared/ui/ui.module';
import { CustomFormModule } from '../custom-form/custom-form.module';
import { KeycloakAngularModule, KeycloakService } from 'keycloak-angular';
import { FormsModule } from '@angular/forms';


export function initializeKeycloak(keycloak: KeycloakService) {
  return () =>
    keycloak.init({
      config: {
        url: 'http://52.91.106.182:8080/auth',
        realm: 'katalog',
        clientId: 'katalog-admin-fe'
      },
      initOptions: {
        onLoad: 'check-sso',
        silentCheckSsoRedirectUri:
          window.location.origin + '/assets/silent-check-sso.html'
      }
    });
}

@NgModule({
  declarations: [ItemMainComponent],
  imports: [
    CommonModule,
    ItemRoutingModule,
    SharedModule,
    UIModule,
    CustomFormModule,
    FormsModule
  ],
  providers:[
    {
      provide: APP_INITIALIZER,
      useFactory: initializeKeycloak,  
      multi: true,
      deps: [KeycloakService]
    }
  ]
})
export class ItemModule { }
