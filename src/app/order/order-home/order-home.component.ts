import { Component, OnInit } from '@angular/core';
import { OrderService } from 'src/app/core/services/order.service';

@Component({
  selector: 'app-order-home',
  templateUrl: './order-home.component.html',
  styleUrls: ['./order-home.component.scss']
})
export class OrderHomeComponent implements OnInit {

  currentPage: number = 0;
  columns: any[] = [
    { header: "Code", field: "code" },
    { header: "Item Id", field: "itemId" },
    { header: "Email", field: "email" },
    { header: "First Name", field: "firstname" },
    { header: "Last Name", field: "lastname" },
    { header: "Address", field: "address" },
    { header: "Quantity", field: "qty" },
    { header: "Product Price", field: "productprice" },
    { header: "Contact", field: "contact" },
  ];
  tableData: any[] = [];

  constructor(
    private orderService: OrderService
  ) { }

  ngOnInit() {
    this.getOrders(`?page=0&limit=10`);
  }

  getOrders(params) {
    this.orderService.getOrders(params)
      .subscribe(
        (res) => {
          this.tableData = res;
        }
      );
  }

  pageChangeEvent(event) {
    if (event.isPrev || (!event.isPrev && this.tableData.length > 0)) {
      this.currentPage = event.page;
      let params = `?page=${event.page}&limit=${event.size}`;
      //call get order function
      this.getOrders(params);
    }
  }

}
