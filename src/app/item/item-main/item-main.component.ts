import { Component, OnInit } from '@angular/core';
import { Action } from 'src/app/core/models/action';
import { FormService } from 'src/app/core/services/form.service';
import { ItemService } from 'src/app/core/services/item.service';
import { FormItemComponent } from 'src/app/custom-form/form-item/form-item.component';

@Component({
  selector: 'app-item-main',
  templateUrl: './item-main.component.html',
  styleUrls: ['./item-main.component.scss']
})
export class ItemMainComponent implements OnInit {

  searchTerm: string;
  currentPage: number = 0;
  columns: any[] = [
    { header: "#", field: "id" },
    { header: "Title", field: "title" },
    { header: "Description", field: "description" },
    { header: "Specification", field: "specification" },
    { header: "Price", field: "price" },
  ];
  tableData: any[] = [];

  constructor(
    private formService: FormService,
    private itemService: ItemService,
  ) { }

  ngOnInit() {
    let params = `?page=${this.currentPage}&limit=10`;
    this.getItems(params);
  }

  getItems(params: string) {
    this.itemService.getItems(params)
      .subscribe(
        (res) => {
          this.tableData = res;
        },
        (error) => {
          console.log(error);
        }
      );
  }

  search() {
    let params = `?kws=${this.searchTerm}`
    this.getItems(params);
  }
  newItem() {
    this.openForm(Action.ADD, null,"New Item")
  }
  editItem(event) {
    this.openForm(Action.UDPATE, event.data,"Update Item")
  }
  deleteItem(event) {
    this.openForm(Action.DELETE, event.data,"Delete Item")
  }
  viewItem(event) {
    this.openForm(Action.VIEW, event.data,"View Item")
  }
  openForm(type: Action, data: any,header:string) {
    document.body.classList.toggle('right-bar-enabled');
    this.formService.setFormComponent({ formName: header, component: FormItemComponent, type: type, data: data });
    this.formService.afterFormSource.subscribe(
      (res) => {
        let params = `?page=${this.currentPage}&limit=10`;
        this.getItems(params);
      }
    );
  }
  pageChangeEvent(event) {
    if (event.isPrev || (!event.isPrev && this.tableData.length > 0)) {
      this.currentPage = event.page;
      let params = `?page=${event.page}&limit=${event.size}`;
      this.getItems(params);
    }
  }
}
